import com.google.gson.Gson;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by oystein on 14/10/2015.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("lol");
        Gson g = new Gson();

        List<String> read = read();

//        read.stream().limit(10).forEach(Main::transform);
        List<Vin> collect = read.stream().map(s -> transform(s)).collect(Collectors.toList());
        System.out.println(Vin.ukjentCounter + " ukjent av " + collect.size());
        System.out.println(collect.get(0));
        System.out.println(collect.size());





        for (Map.Entry<String, Integer> entry : Vin.map.entrySet()) {
            System.out.println(entry.getKey()+" : "+ entry.getValue());
        }

        writeToFile(g.toJson(collect));

        Collections.sort(collect);
//        collect.stream().limit(3).forEach(System.out::println);
//        writeToFile(g.toJson(collect.stream().limit(1).collect(Collectors.toList())));

//        System.out.println("----------- Best alkohol --------------");
//
//        collect.stream().filter(vin -> vin.Alkohol>20).sorted((v1, v2) -> Double.compare(v2.AlkholPris,v1.AlkholPris)).limit(10)
//                .forEach(System.out::println);


        System.out.println("----------- Best rødvin  --------------");

        collect.stream().filter(v -> !v.Varetype.equals("Alkoholfritt")).sorted((v1, v2) -> Double.compare(v1.AlkholPris, v2.AlkholPris)).limit(10)
                .forEach(System.out::println);


        System.out.println("----------- Best Reverse  --------------");

        collect.stream().filter(v -> !v.Varetype.equals("Alkoholfritt")).sorted((v1, v2) -> Double.compare(v1.AlkholPris, v2.AlkholPris)).limit(50)
                .forEach(System.out::println);





    }
    public static void writeToFile(String s){
        try {
            //write converted json data to a file named "file.json"

            FileWriter writer = new FileWriter(new File("produkter2.json"));
            writer.write(s);
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Vin transform(String s) {
        String[] split = s.split(";");
        if (split.length != 36) {
            System.out.println("error");
            return null;
        }

        return  new Vin(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8], split[9], split[10], split[11], split[12], split[13], split[14], split[15], split[16], split[17], split[18], split[19], split[20], split[21], split[22], split[23], split[24], split[25], split[26], split[27], split[28], split[29], split[30], split[31], split[32], split[33], split[34], split[35]);

    }

    private static List<String> read() throws Exception {
        File fileDir = new File("produkter.csv");

        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(fileDir), "UTF-8"));
        List<String> list = new ArrayList<>();
        try {

            br.readLine();//Skip first
            String line = br.readLine();


            while (line != null) {
                list.add(line);
                line = br.readLine();


            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }


    private static void readStreamOfLinesUsingFiles() throws IOException {

        Path path = Paths.get("/Users/oystein/IdeaProjects/XMLtoJSON2a/src", "produkter.csv");
        try (Stream<String> lines = Files.lines(path, Charset.defaultCharset())) {

            lines.limit(2).forEach(s -> System.out.println(s));

        } catch (IOException ex) {

        }

    }
}
