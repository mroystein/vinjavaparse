import java.util.HashMap;

/**
 * Created by oystein on 14/10/2015.
 */
public class Vin implements Comparable<Vin>{
    String Datotid, Varenavn, Varetype, Produktutvalg, Butikkategori, Farge, Lukt, Smak;

    double Volum, Pris, Literpris, Alkohol, Sukker;

    int Friskhet, Garvestoffer, Bitterhet, Sodme, Fylde,Varenummer;

    String Passertil01, Passertil02, Passertil03;

    String Land, Distrikt, Underdistrikt;

    String Argang;
    String Rastoff;
    String Metode;
    String Syre;
    String Lagringsgrad;
    String Produsent;
    String Grossist;
    String Distributor;
    String Emballasjetype;
    String Korktype;
    String Vareurl;

    double AlkholPris;

    static int ukjentCounter = 0;

    public Vin(String datotid, String varenummer, String varenavn, String volum, String pris, String literpris, String varetype, String produktutvalg, String butikkategori, String fylde, String friskhet, String garvestoffer, String bitterhet, String sodme, String farge, String lukt, String smak, String passertil01, String passertil02, String passertil03, String land, String distrikt, String underdistrikt, String argang, String rastoff, String metode, String alkohol, String sukker, String syre, String lagringsgrad, String produsent, String grossist, String distributor, String emballasjetype, String korktype, String vareurl) {
        Datotid = datotid;
        Varenummer = parseInt(varenummer);
        Varenavn = varenavn;
        Volum = parseDouble(volum);
        Pris = parseDouble(pris);
        Literpris = parseDouble(literpris);
        Varetype = varetype;
        Produktutvalg = produktutvalg;
        Butikkategori = butikkategori;
        Fylde = parseInt(fylde);
        Friskhet = parseInt(friskhet);
        parseInt(friskhet);
        Garvestoffer = parseInt(garvestoffer);
        Bitterhet = parseInt(bitterhet);
        Sodme = parseInt(sodme);
        Farge = farge;
        Lukt = lukt;
        Smak = smak;
        Passertil01 = passertil01;
        Passertil02 = passertil02;
        Passertil03 = passertil03;
        Land = land;
        Distrikt = distrikt;
        Underdistrikt = underdistrikt;
        Argang = argang;
        Rastoff = rastoff;
        Metode = metode;
        Alkohol = parseDouble(alkohol);
        Sukker = parseDouble(sukker);
        Syre = syre;
        Lagringsgrad = lagringsgrad;
        Produsent = produsent;
        Grossist = grossist;
        Distributor = distributor;
        Emballasjetype = emballasjetype;
        Korktype = korktype;
        Vareurl = vareurl;

        if(Alkohol<=0.0001)
            AlkholPris=1000000;
        else
            AlkholPris = (Alkohol/100)*Literpris;
    }

    private double parseDouble(String s) {
        if (s.equals("Ukjent")) {
//            System.out.println(s);
            ukjentCounter++;
            return -1;
        }
        s = s.replace(",", ".");
        return Double.parseDouble(s);
    }

    public static HashMap<String, Integer> map = new HashMap<>();

    private int parseInt(String s) {

        return Integer.parseInt(s);
    }

    public String getDatotid() {
        return Datotid;
    }


    public String print(double d){
        return String.format("%.2f",d);
    }

    @Override
    public String toString() {
        return "" +
                "% Pris='" + print(AlkholPris) + '\'' +

                "\t Pris=" + Pris +
                "\t LiterPris=" + Literpris +
                "\t Alkohol=" + Alkohol +
                "\t L=" + Volum +
                "\t " + Varetype +
                "\t " + Varenavn;
    }

    @Override
    public int compareTo(Vin o) {
        return Double.valueOf(o.AlkholPris).compareTo(AlkholPris);
    }






    //Datotid;Varenummer;Varenavn;Volum;Pris;Literpris;Varetype;Produktutvalg;Butikkategori;Fylde;Friskhet;Garvestoffer;Bitterhet;Sodme;Farge;Lukt;Smak;Passertil01;Passertil02;Passertil03;Land;Distrikt;Underdistrikt;Argang;Rastoff;Metode;Alkohol;Sukker;Syre;Lagringsgrad;Produsent;Grossist;Distributor;Emballasjetype;Korktype;Vareurl
}
